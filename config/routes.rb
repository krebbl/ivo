Rails.application.routes.draw do
  resources :comments
  root :controller => :register, :action => :index

  get '/register/success', to: 'register#success'
  get '/register/:step', to: 'register#show'
  post '/register/:step', to: 'register#create', as: 'register_step'

  get '/register', to: redirect('/register/1')

  resources :registrations, only: [:show, :index, :create]

  namespace :admin do
    root to: redirect('/admin/pupils?semester=current')
    post '/pupils/command' => 'pupils#command'
    post '/companies/command' => 'companies#command'
    post '/schools/command' => 'schools#command'
    post '/semesters/command' => 'school_semesters#command'
    post '/school_class_types/command' => 'school_class_types#command'
    post '/users/command' => 'users#command'
    resource :charts do
      get 'weekly'
    end

    resources :pupils, only: [:new, :create, :show, :destroy, :index, :update] do
      get :checkin, path: 'checkin', to: 'pupils#checkin'
      post :checkin, path: 'checkin', to: 'pupils#do_checkin'
      get :delete_photo, path: 'delete_photo', to: 'pupils#delete_photo'
      get :invoice, path: 'invoice', to: 'pupils#invoice'
      resources :comments, controller: 'pupil_comments' do
        get :delete, to: 'pupil_comments#delete', as: 'delete'
      end
    end
    resources :school_years, only: [:new, :create]
    resources :companies, only: [:new, :create, :show, :destroy, :index, :update] do
      resources :comments, controller: 'comments' do
        get :delete, to: 'comments#delete', as: 'delete'
      end
    end
    resources :schools, only: [:new, :create, :show, :destroy, :index, :update] do
      resources :comments, controller: 'comments' do
        get :delete, to: 'comments#delete', as: 'delete'
      end
    end
    resources :users, only: [:new, :create, :show, :destroy, :index, :update]
    resources :school_semesters, only: [:new, :create, :show, :destroy, :index, :update], path: :semesters
    resources :school_class_types, only: [:new, :create, :show, :destroy, :index, :update] do
      post '/classes/command' => 'school_classes#command'

      resources :classes, only: [:new, :create, :show, :destroy, :index, :update], controller: :school_classes
    end

    get '/login', to: 'sessions#new', as: :login
    post '/login', to: 'sessions#create'
    get '/logout', to: 'sessions#destroy', as: :logout

    resource :dialogs do
      get '/invoice', to: 'dialogs#invoice'
      post '/invoice', to: 'dialogs#create_invoice'

      # get '/export'
      # post '/export', to: 'dialogs#export'

    end

    get '/dashboard', to: 'dashboard#show'
  end

  namespace :api do
    namespace :v1 do
      resources :school_classes, only: [:index], :defaults => {:format => 'json'}
      resources :schools, only: [:show], :defaults => {:format => 'json'}
      resources :companies, only: [:show], :defaults => {:format => 'json'}
      resources :pupils, only: [:update], :defaults => {:format => 'json'}
    end
  end

end
