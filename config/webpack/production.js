process.env.NODE_ENV = process.env.NODE_ENV || 'production'

const environment = require('./environment');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

environment.config.set('devtool', 'cheap-source-map\n');

environment.plugins.prepend(
    'HardSourceWebpackPlugin',
    new HardSourceWebpackPlugin()
);

module.exports = environment.toWebpackConfig();
