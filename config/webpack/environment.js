const webpack = require('webpack');
const path = require('path');

const { environment } = require('@rails/webpacker')

// Add an additional plugin of your choosing : ProvidePlugin
environment.plugins.prepend(
    'Provide',
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        jquery: 'jquery'
    })
);

environment.config.merge({
    module: {
        rules: [
            {
                loader: "webpack-modernizr-loader",
                test: /\.modernizrrc\.js$/
            }
        ]
    },
    resolve: {
        alias: {
            modernizr$: path.resolve(__dirname, "./.modernizrrc.js")
        }
    }
});

module.exports = environment;
