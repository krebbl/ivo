"use strict";

module.exports = {
    minify: false,
    options: [
        "setClasses"
    ],
    "feature-detects": [
        "window/framed"
    ]
};