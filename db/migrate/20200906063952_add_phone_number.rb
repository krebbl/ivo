class AddPhoneNumber < ActiveRecord::Migration[5.1]
  def change
    add_column :pupil_parents, :phone_nr, :string
  end
end
