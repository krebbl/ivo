class AddCommentsCounter < ActiveRecord::Migration[5.1]
  def change
    add_column :companies, :comments_count, :integer
    add_column :schools, :comments_count, :integer
  end
end
