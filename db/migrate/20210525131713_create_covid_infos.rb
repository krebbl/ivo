class CreateCovidInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :covid_infos do |t|
      t.references :pupil
      t.string :status, null: false, default: :unknown
      t.date :tested_on
      t.date :first_vaccination_on
      t.date :second_vaccination_on
      t.timestamps
    end
  end
end
