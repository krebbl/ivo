class AddNationalityColumn < ActiveRecord::Migration[5.1]
  def change
    change_table :pupils do |t|
      t.change :nationality_id, :string
      t.rename :nationality_id, :nationality
    end

    Pupil.all.each do |pupil|
      nationality = case pupil.nationality
                      when "1"
                        nil
                      when "2"
                        "DE"
                      when "3"
                        "CH"
                      when "4"
                        "RS"
                      when "5"
                        "TR"
                      when "6"
                        "AF"
                      when "7"
                        "GR"
                      when "8"
                        "LT"
                      when "9"
                        "IT"
                      when "10"
                        "ES"
                      when "11"
                        "KZ"
                      when "12"
                        "BA"
                      when "14"
                        "IR"
                      when "15"
                        "PL"
                      when "16"
                        "UA"
                      when "17"
                        "SK"
                      when "18"
                        "KG"
                      when "19"
                        "IQ"
                      else
                        pupil.nationality
                    end
      pupil.update_attribute(:nationality, nationality) if nationality
    end
  end
end
