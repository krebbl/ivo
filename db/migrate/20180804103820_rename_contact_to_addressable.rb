class RenameContactToAddressable < ActiveRecord::Migration[5.1]
  def change
    rename_column :addresses, :contact_id, :addressable_id
    rename_column :addresses, :contact_type, :addressable_type
  end
end
