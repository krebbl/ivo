class AddMeaslesVaccinationToPupil < ActiveRecord::Migration[5.1]
  def change
    add_column :pupils, :measles_vaccination, :string
  end
end
