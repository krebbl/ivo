class AddPhotoColumnsToPupils < ActiveRecord::Migration[5.1]
  def change
    add_attachment :pupils, :photo
  end
end
