class RemoveTypeColumnFromBills < ActiveRecord::Migration[5.1]
  def change
    execute 'DELETE FROM pupil_bills WHERE type = "FOOD";'
    
    remove_column :pupil_bills, :type
  end
end
