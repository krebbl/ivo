class FixColumns < ActiveRecord::Migration[5.1]
  def change
    ApplicationRecord.connection.tables.each do |table|
      begin
        model_class = table.classify.constantize
        column_names = model_class.column_names
      rescue
        column_names = []
      end
      if table.to_s == 'deposits'
        Deposit.where("paid_in < '0001-01-01'").update_all(paid_in: "0001-01-01")
        Deposit.where("paid_out < '0001-01-01'").update_all(paid_out: "0001-01-01")

        sql = "ALTER TABLE `deposits`"
        sql << "  CHANGE `paid_in` `paid_in` date NULL DEFAULT NULL"
        sql << ", CHANGE `paid_out` `paid_out` date NULL DEFAULT NULL"
        sql << ";"
        execute sql

        Deposit.where({paid_in: "0001-01-01"}).update_all(paid_in: nil)
        Deposit.where({paid_out: "0001-01-01"}).update_all(paid_out: nil)
      end

      if column_names.include?('created')
        execute "UPDATE #{table} SET created = NULL where created < '0001-01-01';"
        execute "UPDATE #{table} SET modified = NULL where modified < '0001-01-01';"

        change_table table do |t|
          t.change_default :created, nil
          t.change_default :modified, nil

          t.rename :created, :created_at
          t.rename :modified, :updated_at
        end
      end

      if column_names.include?('arrival_day')
        execute "UPDATE #{table} SET arrival_day = NULL where arrival_day < '0001-01-01';"
      end

    end
  end
end
