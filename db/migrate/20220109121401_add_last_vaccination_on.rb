class AddLastVaccinationOn < ActiveRecord::Migration[5.1]
  def change
    add_column :covid_infos, :last_vaccination_on, :date
  end
end
