class AddCommentCounterCacheToPupil < ActiveRecord::Migration[5.1]
  def change
    add_column :pupils, :comments_count, :integer, default: 0
  end
end
