class FixContactTypeForAddresses < ActiveRecord::Migration[5.1]
  def up
    Address.all.each do |address|
      contact_type = case address.contact_type
                       when "parent"
                       when "pupilparent"
                         "PupilParent"
                       when "pupil"
                         "Pupil"
                       when "company"
                         "Company"
                       when "school"
                         "School"
                       else
                         nil
                     end
      address.update_attribute(:contact_type, contact_type) if contact_type
    end
  end
end
