class FixColumnLengthForPassword < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :password_digest, :string, :limit => 255
  end
end
