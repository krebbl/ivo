class CompanyNotesToComments < ActiveRecord::Migration[5.1]
  def change
    Company.where("notes > ''").each do |company|
      Comment.create!(
          commentable: company,
          text: company.notes,
          user: User.find_by(username: "andreask")
      )
      company.update(notes: nil)
    end
  end
end
