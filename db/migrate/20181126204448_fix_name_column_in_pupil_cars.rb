class FixNameColumnInPupilCars < ActiveRecord::Migration[5.1]
  def change
    change_column :pupil_cars, :name, :string, :limit => 255
  end
end
