# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20220109121401) do

  create_table "acos", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.integer "parent_id"
    t.string "model"
    t.integer "foreign_key"
    t.string "alias"
    t.integer "lft"
    t.integer "rght"
  end

  create_table "addresses", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.text "contact"
    t.string "street"
    t.string "city"
    t.string "zipcode"
    t.string "home_nr"
    t.string "fax_nr"
    t.string "mobile_nr"
    t.integer "addressable_id"
    t.string "addressable_type"
    t.index ["addressable_type", "addressable_id"], name: "contact_type", unique: true
  end

  create_table "aros", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.integer "parent_id"
    t.string "model"
    t.integer "foreign_key"
    t.string "alias"
    t.integer "lft"
    t.integer "rght"
  end

  create_table "aros_acos", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.integer "aro_id", null: false
    t.integer "aco_id", null: false
    t.string "_create", limit: 2, default: "0", null: false
    t.string "_read", limit: 2, default: "0", null: false
    t.string "_update", limit: 2, default: "0", null: false
    t.string "_delete", limit: 2, default: "0", null: false
    t.index ["aro_id", "aco_id"], name: "ARO_ACO_KEY", unique: true
  end

  create_table "comments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3" do |t|
    t.text "text"
    t.integer "commentable_id"
    t.string "commentable_type"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "companies", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "name"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.text "notes"
    t.integer "comments_count"
  end

  create_table "counties", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "name"
    t.integer "state_id"
  end

  create_table "covid_infos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3" do |t|
    t.bigint "pupil_id"
    t.string "status", default: "unknown", null: false
    t.date "tested_on"
    t.date "first_vaccination_on"
    t.date "second_vaccination_on"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "last_vaccination_on"
    t.index ["pupil_id"], name: "index_covid_infos_on_pupil_id"
  end

  create_table "deposits", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "nr"
    t.string "block"
    t.integer "value"
    t.date "paid_in"
    t.date "paid_out"
    t.integer "pupil_id"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.index ["pupil_id"], name: "pupil_index", unique: true
  end

  create_table "groups", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "name", limit: 100, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "nationalities", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "name"
  end

  create_table "pupil_bills", id: :integer, unsigned: true, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.integer "pupil_id"
    t.string "cw", limit: 11
    t.string "value", limit: 11
  end

  create_table "pupil_cars", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "name"
    t.string "color"
    t.string "sign"
    t.string "key"
    t.integer "pupil_id"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.index ["pupil_id"], name: "pupil_index", unique: true
  end

  create_table "pupil_comments", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.text "text", limit: 4294967295
    t.integer "pupil_id"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.integer "user_id"
    t.index ["pupil_id", "id"], name: "comment_pupil_index", unique: true
  end

  create_table "pupil_parents", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "firstname"
    t.string "lastname"
    t.integer "pupil_id"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.string "phone_nr"
    t.index ["pupil_id"], name: "pupil_index"
  end

  create_table "pupil_wifis", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "token1", limit: 11
    t.string "token2"
    t.integer "pupil_id"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.index ["pupil_id"], name: "pupil_index", unique: true
  end

  create_table "pupils", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "firstname"
    t.string "lastname"
    t.boolean "male"
    t.string "email"
    t.string "birthplace"
    t.date "birthdate"
    t.integer "splitted_custody", limit: 1, default: 0, null: false, unsigned: true
    t.integer "county_id"
    t.date "arrival_day"
    t.integer "min_to_arrive", default: 0
    t.integer "min_to_depart", default: 0
    t.boolean "permanent", default: false
    t.string "room", limit: 50
    t.string "subclass", limit: 10
    t.string "passwd"
    t.string "nationality"
    t.integer "school_class_id"
    t.integer "school_id"
    t.integer "company_id"
    t.date "checked_out"
    t.text "information"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.boolean "rent_on_account", default: false, null: false
    t.integer "food_on_account", limit: 1, default: 0, null: false
    t.boolean "banished", default: false
    t.string "img_url"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer "comments_count", default: 0
    t.string "measles_vaccination"
    t.index ["school_class_id"], name: "school_class_index"
  end

  create_table "school_class_extensions", primary_key: "abbreviation", id: :string, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "name"
  end

  create_table "school_class_types", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "abbreviation", default: "", null: false
    t.string "name"
    t.integer "duration", default: 6, unsigned: true
    t.integer "permanent", limit: 1, default: 0, unsigned: true
    t.index ["abbreviation"], name: "abbreviation", unique: true
  end

  create_table "school_classes", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.integer "nr"
    t.integer "school_class_type_id"
    t.integer "start_semester"
    t.string "allocation", default: ""
    t.datetime "updated_at"
    t.datetime "created_at"
    t.index ["start_semester", "school_class_type_id"], name: "school_class_type_semester", unique: true
  end

  create_table "school_semesters", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.date "start_date", default: "2008-08-01"
    t.date "stop_date", default: "2009-01-31"
  end

  create_table "schools", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "name"
    t.string "abbreviation", limit: 30
    t.datetime "updated_at"
    t.datetime "created_at"
    t.integer "comments_count"
  end

  create_table "states", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "name"
  end

  create_table "users", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8mb3" do |t|
    t.string "username", null: false
    t.string "password_digest", null: false
    t.integer "group_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "can_export", default: false, null: false
    t.index ["username"], name: "username", unique: true
  end

end
