require 'test_helper'

class PupilTest < ActiveSupport::TestCase
  test "parents validation" do
    pupil = Pupil.new
    pupil.assign_attributes({
                                parents_attributes: [],
                                birthdate: 15.years.ago
                            })
    pupil.validate

    assert_not_empty(pupil.errors.messages[:parents])

    pupil = Pupil.new
    pupil.assign_attributes({
                                parents_attributes: [],
                                birthdate: (18.years + 1.day).ago
                            })
    pupil.validate

    assert_empty(pupil.errors.messages[:parents])
  end
end
