FROM ruby:2.6.5-stretch

# Install mysql.
RUN \
  apt-get update && \
  apt-get install -y \
  apt-transport-https mysql-client

# Install nodejs
RUN apt-get update && apt-get install -y curl apt-transport-https ca-certificates && \
    curl --fail -ssL -o setup-nodejs https://deb.nodesource.com/setup_12.x && \
    bash setup-nodejs && \
    apt-get install -y nodejs build-essential

# Install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn

RUN mkdir /app

VOLUME /app/test/reports

WORKDIR /app

COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock

RUN gem install bundler
RUN gem install pkg-config

RUN bundle install

COPY package.json /app/.
COPY yarn.lock /app/.
RUN yarn install

COPY . /app

RUN rails assets:precompile
