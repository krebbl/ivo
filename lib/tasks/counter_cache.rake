desc 'Counter cache for pupil has many comments'

task comment_counter: :environment do
  Pupil.reset_column_information
  Pupil.all.each do |p|
    Pupil.reset_counters p.id, :comments
  end
end