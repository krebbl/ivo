namespace :import do

  task :photos, [:path] => :environment do |t, args|
    args.with_defaults(:path => "./tmp/photos/*.jpg")
    entries = Dir[args.path]
    puts "Found #{entries.length} entries in #{args.path}"
    entries.each do |file|
        pupil_id = File.basename(file, ".*")
        begin
          pupil = Pupil.find(pupil_id)
          handle = File.open(file)
          pupil.photo = handle
          handle.close
          pupil.save!
        rescue

        end
    end
  end

  task :nationality, [:path] => :environment do
    Pupil.all.each do |pupil|
      nationality = case pupil.nationality
                    when "1"
                      nil
                    when "2"
                      "DE"
                    when "3"
                      "CH"
                    when "4"
                      "RS"
                    when "5"
                      "TR"
                    when "6"
                      "AF"
                    when "7"
                      "GR"
                    when "8"
                      "LT"
                    when "9"
                      "IT"
                    when "10"
                      "ES"
                    when "11"
                      "KZ"
                    when "12"
                      "BA"
                    when "14"
                      "IR"
                    when "15"
                      "PL"
                    when "16"
                      "UA"
                    when "17"
                      "SK"
                    when "18"
                      "KG"
                    when "19"
                      "IQ"
                    else
                      pupil.nationality
                    end
      pupil.update_attribute(:nationality, nationality) if nationality
      puts pupil.nationality
    end
  end

end
