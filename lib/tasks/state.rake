require 'csv'

namespace :state do
  task import: [:environment] do
    csv_text = File.read('staatenliste.csv')
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "    #{row['ISO 3166-2']}: #{row['Staatsname amtliche Kurzform']}"
      # Moulding.create!(row.to_hash)
    end
  end

end
