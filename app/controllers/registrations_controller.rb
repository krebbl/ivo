class RegistrationsController < ApplicationController
  layout 'register'

  def index

  end

  def create
    @pupil = Pupil.new
    @pupil.assign_attributes(params.require(:pupil).permit(:firstname, :lastname, :passwd))
    redirect_to registration_path(@pupil.public_id)
  end

  def show
    id = params[:id]
    @pupil = Pupil.find_by_public_id(id)
    if @pupil
      respond_to do |format|
        format.html
        format.pdf do
          filename = "#{@pupil.lastname}_#{@pupil.firstname}"
          disposition = params.has_key?(:inline) ? 'inline' : "attachment; filename=#{filename}.pdf"
          pdf = RegistrationPdf.new(@pupil)

          send_data pdf.render,
                    filename: filename,
                    type: 'application/pdf',
                    disposition: disposition
        end
      end
    else
      redirect_to register_path
    end
  end
end
