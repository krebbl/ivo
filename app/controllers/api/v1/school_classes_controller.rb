class Api::V1::SchoolClassesController < ApplicationController

  def index
    school_classes = SchoolClass.with_type_and_semester

    if params[:school_class_type]
      school_classes = school_classes.where(school_class_type_id: params[:school_class_type])
    end

    render json: school_classes, :include => [:type, :semester], :methods => [:option_name], :except => [:school_class_type_id, :start_semester], status: 200
  end

end
