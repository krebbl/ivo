class Api::V1::PupilsController < ApplicationController
  include AuthenticationSystem
  before_action :login_required

  def update
    pupil = Pupil.find(params[:id])
    pupil.update(permitted_parameters)
  end

  def permitted_parameters
    params.permit(:room, :firstname, :lastname, :subclass)
  end
end
