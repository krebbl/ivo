class Api::V1::CompaniesController < ApplicationController
  def show
    @company = Company.with_address.find(params[:id])

    respond_to do |format|
      format.html {render 'show', layout: false }
      format.json {render json: @company, :include => [:address], status: 200}
    end
  end
end
