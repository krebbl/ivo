class Api::V1::SchoolsController < ApplicationController

  def show
    @school = School.with_address.find(params[:id])

    respond_to do |format|
      format.html {render 'show', layout: false}
      format.json {render json: @school, :include => [:address], status: 200}
    end
  end

end
