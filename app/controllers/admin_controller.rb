class AdminController < ApplicationController
  include AuthenticationSystem
  include GroupCommand

  protect_from_forgery with: :exception

  before_action :login_required
  before_action :save_referer

  add_flash_types :error, :success, :warning, :info, :danger

  def save_referer
    if action_name == 'index'
      @back_link = request.referer
    else
      @back_link = request.referer
    end
  end

end