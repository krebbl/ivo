class Admin::SessionsController < AdminController
  protect_from_forgery :except => [:create]

  before_action :check_login

  skip_before_action :login_required, only: [:new, :create]

  skip_before_action :check_login, only: [:destroy]

  def create
    user = User.find_by(username: params[:username])
    if user && user.authenticate(params[:password])
      sign_in(user)
      redirect_to admin_dashboard_path
    else
      flash.now[:danger] = 'Passwort oder Benutzername falsch!'
      render :new
    end
  end

  def destroy
    flash[:success] = 'Erfolgreich abgemeldet!'
    sign_out
    redirect_to(admin_login_path)
  end

  private

  def check_login
    if logged_in?
      redirect_to admin_pupils_path({semester: :current})
    end
  end
end
