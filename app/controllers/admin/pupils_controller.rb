class Admin::PupilsController < AdminController

  def new
    @pupil = Pupil.new

    load_data

    render :show
  end

  def create
    @pupil = Pupil.new

    @pupil.assign_attributes(pupil_params)

    if @pupil.save
      redirect_to(admin_pupil_url(@pupil), success: 'Schüler erfolgreich gespeichert.')
    else
      flash.now[:alert] = @pupil.errors.full_messages.first

      load_data

      render :show
    end
  end

  def update
    @pupil = Pupil.find(params[:id])

    @pupil.assign_attributes(pupil_params)

    if @pupil.save
      redirect_to(admin_pupil_url(@pupil), success: 'Schüler erfolgreich gespeichert.')
    else
      flash.now[:alert] = @pupil.errors.full_messages.first

      load_data

      render :show
    end

  end

  def delete_photo
    @pupil = Pupil.find(params[:pupil_id])

    @pupil.photo = nil

    @pupil.save

    redirect_to(admin_pupil_url(@pupil), success: 'Foto erfolgreich gelöscht.')
  end

  def edit
    @pupil = Pupil.find(params[:id])

    load_data
  end

  def show
    @pupil = Pupil.find(params[:id])

    load_data

    respond_to do |format|
      format.html
      format.pdf do
        filename = "#{@pupil.lastname}_#{@pupil.firstname}"
        disposition = 'inline'
        pdf = RegistrationPdf.new(@pupil)

        send_data pdf.render,
                  filename: filename,
                  type: 'application/pdf',
                  disposition: disposition
      end
    end
  end

  def index
    @pupils = Pupil.with_details.order(lastname: :ASC)
    @companies = Company.order(name: :ASC)

    if params[:semester] == 'current'
      @semester = SchoolSemester.latest
    elsif params[:semester].present?
      @semester = SchoolSemester.find(params[:semester])
    else
      @semester = nil
    end

    filters = params.permit(:cw, :search, :status, :deposit, :company, :school_class_type, :county)
    filters[:semester] = @semester

    @pupils = PupilFilter.filter(filters, @pupils)
    @companies = @companies.where(id: @pupils.map {|p| p.company_id})
    @semesters = SchoolSemester.order(start_date: :DESC).all

    @deposit_options = [
        ['Alle', :all],
        ['Noch nicht eingezahlt', :not_paid_in],
        ['Noch nicht ausgezahlt', :not_paid_out],
        ['Ausgezahlt', :paid_out]
    ]
  end


  def export(ids)
    xls = PupilListXLS.new(ids)

    send_data xls.render,
              filename: "Export_#{Date.today}.xlsx",
              type: 'application/excel',
              disposition: 'inline'
  end

  def load_data
    @companies = Company.with_address.order(name: :ASC).all
    @schools = School.with_address.order(name: :ASC).all
    @school_class_types = SchoolClassType.order(abbreviation: :ASC).all
    @counties = County.order(name: :ASC).all
    @school_classes = @pupil.school_class ? @pupil.school_class.type.school_classes.with_type_and_semester.all : []
  end

  def checkin
    @pupil = Pupil.find(params[:pupil_id])

  end


  private

  def pupil_params
    address_attributes = [:street, :city, :zipcode, :home_nr, :mobile_nr, :fax_nr]

    params
        .require(:pupil).permit(
        :firstname, :lastname,
        :male, :email, :photo,
        :splitted_custody,
        :arrival_day,
        {address_attributes: address_attributes},
        {deposit_attributes: [:block, :nr, :paid_in, :paid_out, :value]},
        {car_attributes: [:name, :sign, :color, :key]},
        {wifi_attributes: [:token1, :token2]},
        {parents_attributes: [:_destroy, :id, :phone_nr, :firstname, :lastname, {address_attributes: [:_destroy].concat(address_attributes)}]},
        {bills_attributes: [:cw, :value]},
        {covid_info_attributes: [:status, :first_vaccination_on, :second_vaccination_on, :tested_on, :last_vaccination_on]},
        :school_class_id,
        :company_id, :school_id,
        :create_company, {new_company: [:name, address_attributes: address_attributes]},
        :create_school, {new_school: [:name, address_attributes: address_attributes]},
        :birthplace, :birthdate,
        :nationality, :county_id,
        :measles_vaccination,
        :rent_on_account,
        :food_on_account,
        :checked_out,
        :permanent,
        :banished,
        :room,
        :min_to_arrive,
        :min_to_depart
    )
  end

end
