class Admin::UsersController < AdminController
  before_action :admin_required

  before_action :load_groups, only: [:show, :update, :new, :create]

  def index
    @users = User.order(username: :ASC)
  end

  def show
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    user_hash = params.require(:user).permit(:group_id, :password, :password_confirmation, :can_export)

    @user.assign_attributes user_hash

    if @user.save
      flash[:success] = 'Nutzer erfolgreich gespeichert'
      redirect_to [:admin, @user]
    else
      flash.now[:danger] = @user.errors.full_messages.first

      render :show
    end

  end

  def create
    @user = User.new
    user_hash = params.require(:user).permit(:username, :group_id, :password, :password_confirmation)

    @user.assign_attributes user_hash

    if @user.save
      flash[:success] = 'Nutzer erfolgreich angelegt'
      redirect_to admin_users_path
    else
      flash.now[:danger] = @user.errors.full_messages.first

      render :show
    end
  end

  def new
    @user = User.new

    render :show
  end

  private

  def load_groups
    @groups = Group.order(name: :ASC)

  end

end
