class Admin::SchoolsController < Admin::OrganisationsController
  def model_class
    School
  end

  def model_name
    'Schule'
  end

  def model_name_plural
    'Schulen'
  end

end
