class Admin::OrganisationsController < AdminController
  def index
    @organisations = model_class.with_address.order(name: :ASC)
    @title = model_name_plural

    if params.has_key?(:search)
      p = {search: "%#{params[:search].downcase}%"}
      @organisations = @organisations.joins(:address).where("lower(name) LIKE :search OR lower(addresses.city) LIKE :search", p)
    end

    @total = @organisations.count
    # @organisations = @organisations.page(params[:page])
  end

  def show
    @organisation = model_class.with_address.find(params[:id])
  end

  def create
    @organisation = model_class.new
    @title = "Neue #{model_name} erstellen"

    @organisation.assign_attributes(organisation_params)

    if @organisation.save
      redirect_to url_for([:admin, model_class]), success: "#{model_name} erfolgreich gespeichert."
    else
      render 'new'
    end
  end

  def update
    @organisation = model_class.with_address.find(params[:id])

    @organisation.assign_attributes(organisation_params)

    if @organisation.save
      redirect_to url_for([:admin, model_class]), success: "#{model_name} erfolgreich gespeichert."
    else
      render 'show'
    end
  end

  def new
    @organisation = model_class.new
    @title = "Neue #{model_name} erstellen"
  end

  def model_class
    Organisation
  end

  def model_name
    'Organisation'
  end

  def model_name_plural
    'Organisationen'
  end

  private

  def organisation_params
    params.require(model_class.to_s.parameterize).permit(:name, :notes, address_attributes: [:city, :street, :zipcode, :home_nr, :mobile_nr, :fax_nr, :contact])
  end
end
