class Admin::SchoolClassesController < AdminController

  before_action :load_semesters, only: [:show, :new, :create, :update]

  def index
    @school_class_type = SchoolClassType.find(params[:school_class_type_id])
    @school_classes = SchoolClass.where(school_class_type_id: params[:school_class_type_id]).ordered
  end

  def show
    @school_class = SchoolClassType.find(params[:school_class_type_id]).school_classes.find(params[:id])
  end

  def new
    @school_class = SchoolClass.new(school_class_type_id: params[:school_class_type_id])

    render 'show'
  end

  def create
    @school_class = SchoolClass.new(school_class_type_id: params[:school_class_type_id])

    @school_class.assign_attributes(school_class_params)

    if @school_class.save
      flash[:success] = 'Ausbildungsklasse erfolgreich gespeichert.'
      redirect_to admin_school_class_type_classes_path(@school_class.type)
    else
      render 'show'
    end

  end

  def update
    @school_class = SchoolClassType.find(params[:school_class_type_id]).school_classes.find(params[:id])
    @school_class.assign_attributes(school_class_params)

    if @school_class.save
      flash[:success] = 'Ausbildungsklasse erfolgreich gespeichert.'
      redirect_to admin_school_class_type_classes_path(@school_class.type)
    else
      render 'show'
    end
  end

  private

  def school_class_params
    params.require(:school_class).permit(:start_semester, allocation: [])
  end

  def load_semesters
    @semesters = SchoolSemester.order(start_date: :DESC)
  end
end
