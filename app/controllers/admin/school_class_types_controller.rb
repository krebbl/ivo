class Admin::SchoolClassTypesController < AdminController
  def index
    @school_class_types = SchoolClassType.order(name: :ASC)
  end

  def show
    @school_class_type = SchoolClassType.find(params[:id])
  end

  def new
    @school_class_type = SchoolClassType.new

    render 'show'
  end

  def create
    @school_class_type = SchoolClassType.new

    @school_class_type.assign_attributes(school_class_type_params)

    if @school_class_type.save
      flash[:success] = "Ausbildungsklasse erfolgreich angelegt."
      redirect_to admin_school_class_types_path
    else
      render 'show'
    end

  end

  def update
    @school_class_type = SchoolClassType.find(params[:id])
    @school_class_type.assign_attributes(school_class_type_params)

    if @school_class_type.save
      (params[:school_class_allocations]).each do |id, value|
        school_class = SchoolClass.find(id)
        school_class.allocation = value.select {|v| !v.blank?}
        school_class.save
      end
      redirect_back fallback_location: :admin_school_class_type_classes_path
    else
      render 'show'
    end
  end

  private

  def school_class_type_params
    params.require(:school_class_type).permit(:name, :abbreviation, :permanent, :duration)
  end
end
