class Admin::DashboardController < Admin::OrganisationsController
  def show
    @pupils = Pupil.limit(10).order(created_at: :desc)
    @comments = PupilComment.where("pupil_id IS NOT NULL").includes(:pupil, :user).order(created_at: :desc).limit(5)
  end
end
