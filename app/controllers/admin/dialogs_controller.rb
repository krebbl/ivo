class Admin::DialogsController < ApplicationController
  layout false


  def invoice
    pupil_ids = (params[:ids] || "").split(",")
    pupils = Pupil.where(id: pupil_ids)
    company_ids = pupils.map {|p| p.company_id}.select {|id| !!id}
    addresses = Address.where(addressable_type: 'Company', addressable_id: company_ids)
    start_time = 2.weeks.from_now
    @time_options = (0..40).map {|week|
      time = (start_time - week.weeks)
      [time.strftime('%W.KW %Y'), time.to_i]
    }
    @addresses = addresses.map {|a|
      contact = a.contact || a.addressable.name
      ["#{contact} / #{a.street} in #{a.zipcode} #{a.city}", a.id]
    }
    @invoice = Admin::Invoice.new
    @invoice.pupil_ids = pupil_ids
    @invoice.from = 4.weeks.ago.to_i

    respond_to do |format|
      format.html
      format.js {
        render layout: false
      }
    end
  end

  def create_invoice
    invoice_params = params.require(:invoice).permit(:pupil_ids, {pupil_ids: []}, :address_id, :from, :to, :nr)

    pupils_where = Pupil.where(id: invoice_params[:pupil_ids]).includes(:bills)
    address = Address.find_by(id: invoice_params[:address_id]) if invoice_params[:address_id]
    invoice = InvoiceXLS.new(pupils_where, address, Time.at(invoice_params[:from].to_i), Time.at(invoice_params[:to].to_i), invoice_params[:nr])

    send_data invoice.render,
              filename: "Rechnung_#{Date.today.year}_#{invoice_params[:nr] || 1}.xlsx",
              type: 'application/excel',
              disposition: 'inline'
  end

end
