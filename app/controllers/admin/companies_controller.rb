class Admin::CompaniesController < Admin::OrganisationsController
  def model_class
    Company
  end

  def model_name
    'Firma'
  end

  def model_name_plural
    'Firmen'
  end

end
