class Admin::PupilCommentsController < AdminController

  def index
    @pupil = Pupil.find(params[:pupil_id])
    @pupil_comment = PupilComment.new
  end

  def create
    @pupil = Pupil.find(params[:pupil_id])
    @pupil_comment = PupilComment.new({
                                          pupil: @pupil,
                                          user: current_user})

    @pupil_comment.assign_attributes params.require(:comment).permit(:text)
    if @pupil_comment.save
      redirect_to admin_pupil_comments_path @pupil.id
    else
      flash.now[:error] = @pupil_comment.errors.full_messages.first

      render :index
    end

  end

  def delete
    @pupil = Pupil.find(params[:pupil_id])
    @pupil_comment = @pupil.comments.find(params[:comment_id])

    @pupil_comment.destroy

    redirect_to admin_pupil_comments_path @pupil.id
  end

  def update
    @pupil = Pupil.find(params[:pupil_id])
    @pupil_comment = @pupil.comments.find(params[:id])

    @pupil_comment.assign_attributes params.require(:comment).permit(:text)

    if @pupil_comment.save
      redirect_to admin_pupil_comments_path @pupil.id
    else
      flash.now[:error] = @pupil_comment.errors.full_messages.first

      render :index
    end
  end

end
