class Admin::SchoolYearsController < AdminController
  def new
    latest_semester = SchoolSemester.where("YEAR(start_date) = YEAR(stop_date)").order(start_date: :DESC).first
    @school_year = SchoolYear.new(latest_semester ? latest_semester.stop_date + 2.weeks : nil)
    load_school_class_types
  end

  def create
    @school_year = SchoolYear.new
    school_year_params = params
                             .require(:school_year).
        permit({winter_semester: [:start_date, :stop_date]},
               {summer_semester: [:start_date, :stop_date]},
               {school_class_types: []})

    @school_year.assign_attributes(school_year_params)
    if @school_year.save
      flash[:success] = "Schuljahr erfolgreich angelegt"

      redirect_to admin_school_semesters_path
    else
      flash.now[:alert] = @school_year.errors.full_messages.first

      load_school_class_types

      render :new
    end
  end

  private
  def load_school_class_types
    @school_class_types = SchoolClassType.order(abbreviation: :ASC)

  end
end
