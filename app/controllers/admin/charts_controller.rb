class Admin::ChartsController < ApplicationController
  layout 'chart'

  def weekly
    current_cw = SchoolClass.current_cw
    cws = (0...6).map do |i|
      cw = (current_cw.to_i + i)
      cw > 52 ? cw % 52 : cw
    end
    current_semester = SchoolSemester.latest
    pupil_cws = cws.map do |cw|
      filters = ActionController::Parameters.new(
          semester: current_semester,
          status: 'checked_in',
          cw: cw)
      PupilFilter.filter(filters)
    end
    males = pupil_cws.map do |pupils|
      pupils.select {|p| p.male}.count
    end
    females = pupil_cws.map do |pupils|
      pupils.select {|p| !p.male}.count
    end
    all = pupil_cws.map do |pupils|
      pupils.count
    end
    cws = cws
    series = [{
                   name: 'Alle',
                   data: all
               }, {
                   name: 'Männlich',
                   data: males
               }, {
                   name: 'Weiblich',
                   data: females
               }]


    @chart = {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Wöchentliche Auslastung'
        },
        xAxis: {
            categories: cws.map {|cw| "#{cw}.KW"}
        },
        yAxis: {
            title: {
                text: 'Anzahl Azubis'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: series
    }

    render :show
  end
end
