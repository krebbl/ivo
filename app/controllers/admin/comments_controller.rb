class Admin::CommentsController < AdminController
  def index
    @commentable = get_commentable
    @comment = Comment.new
  end

  def create
    @commentable = get_commentable
    @comment = Comment.new({
                                           commentable: @commentable,
                                           user: current_user})

    @comment.assign_attributes params.require(:comment).permit(:text)
    if @comment.save
      redirect_to url_for([:admin, @commentable, :comments])
    else
      flash.now[:error] = @comment.errors.full_messages.first

      render :index
    end

  end

  def delete
    @commentable = get_commentable
    @comment = @commentable.comments.find(params[:comment_id])

    @comment.destroy

    redirect_to url_for([:admin, @commentable, :comments])
  end

  def update
    @commentable = get_commentable
    @comment = @commentable.comments.find(params[:id])

    @comment.assign_attributes params.require(:comment).permit(:text)

    if @comment.save
      redirect_to url_for([:admin, @commentable, :comments])
    else
      flash.now[:error] = @comment.errors.full_messages.first

      render :index
    end
  end

  private

  def get_commentable
    return Company.find(params[:company_id]) if params[:company_id]
    return School.find(params[:school_id]) if params[:school_id]
    Pupil.find(params[:pupil_id]) if params[:pupil_id]
  end
end
