class Admin::SchoolSemestersController < AdminController
  def index
    @semesters = SchoolSemester.order(start_date: :DESC)
  end

  def show
    @semester = SchoolSemester.find(params[:id])
  end

  def new
    @semester = SchoolSemester.new

    render 'show'
  end

  def create
    @semester = SchoolSemester.find(params[:id])

    @semester.assign_attributes(semester_params)

    if @semester.save
      redirect_to admin_school_semester_path(@semester)
    else
      render 'show'
    end

  end

  def update
    @semester = SchoolSemester.find(params[:id])
    @semester.assign_attributes(semester_params)

    if @semester.save
      redirect_to admin_school_semester_path(@semester)
    else
      render 'show'
    end
  end

  private

  def semester_params
    params.require(:school_semester).permit(:start_date, :stop_date)
  end
end
