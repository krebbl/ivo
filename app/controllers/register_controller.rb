class RegisterController < ApplicationController
  layout 'register'

  def index

    redirect_to register_path(1)
  end

  def show
    @step = params[:step].to_i

    completed_step

    pupil

    load_data

    if @step > completed_step + 1
      redirect_to register_step_path(completed_step + 1)
    else
      render :show
    end

  end

  def create
    @step = params[:step].to_i

    pupil

    if validate_step(@step)
      session[:completed_step] = @step
      if @step < 5
        redirect_to register_step_path(@step + 1)
      else
        session[:completed_step] = nil
        session[:register] = nil
        redirect_to register_success_path(id: @pupil.public_id)
      end
    else
      session[:completed_step] = @step - 1
      load_data
      # flash[:danger] = 'Fehler beim Ausfüllen. Bitte überprüfe die rotmarkierten Felder!'
      render :show
    end
  end

  def success
    public_id = params[:id]
    @pupil = Pupil.find_by_public_id(public_id)

    if @pupil

    else
      redirect_to register_path
    end


  end

  private

  def completed_step
    session[:completed_step] ||= 0
  end

  def validate_step step
    address_attributes = [:_destroy, :street, :zipcode, :city, :home_nr, :fax_nr, :mobile_nr]
    case step
      when 1
        permit_params(:firstname, :lastname, :birthplace, :male, :birthdate, :nationality, :county_id, address_attributes: address_attributes)
      when 2
        permit_params(:splitted_custody, parents_attributes: [:_destroy, :firstname, :lastname, :phone_nr, address_attributes: address_attributes])
      when 3
        permit_params(:company_id, :school_id, :school_class_type_id, :school_semester_id, :subclass, :create_company, {new_company: [:name, address_attributes: address_attributes]}, :create_school, {new_school: [:name, address_attributes: address_attributes]})
      when 4
        permit_params(
          :arrival_day,
          :permanent,
          :min_to_arrive,
          :min_to_depart,
          :information,
          :measles_vaccination,
          { covid_info_attributes: [:status, :first_vaccination_on, :second_vaccination_on, :tested_on, :last_vaccination_on] }
        )
      when 5
        @pupil = pupil
        @pupil.passwd = (0...6).map {(65 + rand(26)).chr}.join
        @pupil.save
      else
        false
    end
  end

  def pupil
    @pupil = Pupil.new
    session_steps.each do |key, value|
      @pupil.assign_attributes(value)
    end
    @pupil
  end

  def permit_params(*args)
    @pupil = pupil
    pupil_params = params.require(:pupil).permit(*args)
    @pupil.assign_attributes(pupil_params)
    @pupil.validate
    error_index = args.index do |attribute_name|
      key = attribute_name
      if attribute_name.is_a? Hash
        key = attribute_name.keys.first.to_s.gsub('_attributes', '').to_sym
      end
      # key = key.to_s.gsub('_id', '').to_sym
      @pupil.errors.messages[key].any?
    end
    session_steps[@step] = pupil_params

    error_index == nil ? true : false
  end

  def session_steps
    session[:register] ||= {}
  end

  def load_data
    @companies = Company.with_address.order(name: :ASC).all
    @schools = School.with_address.order(name: :ASC).all
    @school_class_types = SchoolClassType.order(abbreviation: :ASC).all
    @school_semesters = SchoolSemester.where("start_date >= ?", 3.years.ago).order(start_date: :DESC).all
    @counties = County.order(name: :ASC).all
  end


end
