module GroupCommand
  def command
    command_params = params.require(:command).permit(:name)
    ids = params.dig(:command, :ids)
    model_class = controller_name.singularize.classify.constantize
    flash = {}
    case command_params[:name]
    when 'delete'
      model_class.where(:id => ids).destroy_all
      flash = {success: "#{model_class.model_name.human} erfolgreich gelöscht"}
      redirect_to(request.referer, flash)
    else
      if self.respond_to? command_params[:name].to_sym
        self.send(command_params[:name].to_sym, ids)
      else
        flash = {success: "Unbekannte Aktion"}
        # TODO: remove
        puts 'Unkown command'
        redirect_to(request.referer, flash)
      end
    end
  end
end