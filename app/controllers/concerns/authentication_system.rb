# frozen_string_literal: true

module AuthenticationSystem
  # Inclusion hook to make #current_user and #logged_in?
  # available as ActionView helper methods.
  def self.included(base)
    base.send :helper_method, :current_user, :logged_in?
  end

  protected

  def login_required
    authorized? || access_denied
  end

  def admin_required
    login_required && is_admin? || access_denied
  end

  # Returns true if the user is logged in, false otherwise
  # Preloads @current_user with the user model if they're logged in.
  def logged_in?
    current_user.present?
  end

  def is_admin?
    current_user.present? && current_user.is_admin?
  end

  def access_denied
    redirect_to admin_login_path(redirect_to: request.original_fullpath), alert: 'Zugriff nicht gestattet!'
  end

  # Accesses the current user from the session.
  def current_user
    @current_user ||= login_from_session
  end

  # Sign in the user
  #
  # updates the session and sets the current_user
  def sign_in(new_user)
    session['current_user_id'] = new_user.id
    @current_user = nil
    current_user
  end

  # Logout for the user
  #
  # kill session and remember me cookie
  def sign_out
    reset_session
    @current_user = nil
  end

  def authorized?
    logged_in?
  end

  def login_from_session
    current_user_id = session['current_user_id']
    User.find_by(id: current_user_id) if current_user_id.present?
  end
end
