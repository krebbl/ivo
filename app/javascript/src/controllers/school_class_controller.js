import $ from 'jquery';
import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ['type','select'];

    connect() {
        // TODO: implement something
    }

    loadClass() {
        const value = this.typeTarget.value;
        const $dropdown = $(this.selectTarget);
        $dropdown.empty();
        $dropdown.append($("<option />").val("-1").text("Auswählen"));
        if (value) {
            $.getJSON(`/api/v1/school_classes?school_class_type=${value}`, function (data) {
                data.forEach((sc) => {
                    $dropdown.append($("<option />").val(sc.id).text(sc.option_name));
                });
            })
        }
    }
}