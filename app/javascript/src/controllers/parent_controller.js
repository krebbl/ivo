import $ from 'jquery';
import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ['address', 'addressCheckbox', 'destroy'];

    connect() {
        const self = this;
        $(this.addressTarget).toggle(!this.addressCheckboxTarget.checked);
        $(this.destroyTarget).on('change', () => self.toggleDestroy());
        $(this.destroyTarget.form).on('reset', () => self.handleReset());
    }

    toggleAddress(e) {
        $(this.addressTarget).toggle(!e.target.checked);
    }

    toggleDestroy() {
        $(this.element).toggleClass('deleted', this.destroyTarget.checked);
    }

    handleReset() {
        $(this.element).toggleClass('deleted', !this.destroyTarget.checked);
    }


}