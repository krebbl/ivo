import $ from 'jquery';
import { Controller } from "stimulus"
import ls from 'local-storage'

export default class extends Controller {
    static targets = ['nav', 'content'];

    connect() {
        const tabHandler = (e) => {
            const $el = $(e.target);
            e.preventDefault();
            window.location.replace(
                '#' + $el.attr('href').replace(/^#/, '')
            );
        };
        $(this.navTarget).find('.nav-link').each((index, el) => {
            $(el).click(tabHandler)
        });
        if(!window.location.hash) {
            const activeTab = ls(this.localStorageKey());
            if(activeTab) {
                window.location.replace(activeTab);
            }
        }
        this.openTab();
        $(window).on('hashchange', () => {
            this.openTab();
        });
    }

    localStorageKey() {
        return window.location.pathname + "_tab"
    }

    openTab() {
        const ref = window.location.hash || $(this.navTarget).find('.nav-link').first().attr('href');
        const activePane = $(this.contentTarget).find('> .tab-pane.active');
        activePane.removeClass('active');
        $(this.element).removeClass(activePane.attr('id'));
        $(this.navTarget).find('> .nav-item > .nav-link.active').removeClass('active');

        const newActivePane = $(this.contentTarget).find(`${ref}-pane`);
        newActivePane.addClass('show active');
        $(this.element).addClass(newActivePane.attr('id'));
        $(this.navTarget).find(`*[href="${ref}"]`).addClass('active');
        ls(this.localStorageKey(), ref);
    }

}
