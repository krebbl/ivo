import $ from 'jquery';
import { Controller } from "stimulus"
import Rails from 'rails-ujs';

export default class extends Controller {

    connect() {
        $(this.element).on('blur', this.handleBlur);
        $(this.element).on('keydown', (e) => {
            if (e.which === 13) {
                e.currentTarget.blur();
            }
        });
    }

    handleBlur = (e) => {
        const node = e.currentTarget;
        const pupilId = node.getAttribute("data-pupil-id");
        const formData = new FormData();
        formData.append(node.getAttribute("name"), node.value)
        Rails.ajax({
            url: `/api/v1/pupils/${pupilId}`,
            type: "PUT",
            data: formData,
            dataType: 'application/json',
            success: function (data) {
                console.log(data);
            }
        });
    }
}
