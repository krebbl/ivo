import 'sticky-table-headers'
import 'jquery-sticky-kit'
import { Controller } from "stimulus"

export default class extends Controller {

    connect() {
        $(this.element).click(this.handleClick);
        this.groupName = $(this.element).attr('data-checkbox-group');
        this.checkboxes().change(this.checkCheckboxes);
    }

    handleClick = (e) => {
        this.checkboxes().each(function(){
            this.checked = e.currentTarget.checked;
        });
    };

    checkboxes() {
        return $(`input[type=checkbox][name='${this.groupName}']`)
    }

    checkCheckboxes = () => {
        const uncheckedBoxes = this.checkboxes().not(':checked');
        $(this.element).get(0).checked = this.checkboxes().length > 0 && uncheckedBoxes.length === 0;
    };
}