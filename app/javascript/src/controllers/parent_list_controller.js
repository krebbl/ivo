import $ from 'jquery';
import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ['parentTpl'];

    connect() {

    }

    addParent() {
        const $parentForms = $(this.element).find('.parent-form');
        const length = $parentForms.length;
        const $tpl = $parentForms.last();
        const $nf = $tpl.clone();
        $nf.insertBefore($tpl);
        $nf.find('*[name^="pupil[new_parent]"]').each((index, el) => {
            const name = $(el).attr('name');
            const id = ($(el).attr('id') || '');
            const newId = (id || '').replace('new_parent', `parents_attributes_${length - 1}`)
            $(el).attr({
                name: name.replace('[new_parent]', `[parents_attributes][${length - 1}]`),
                id: newId
            });

            const $label = $(this.element).find(`[for="${id}"]`);
            if($label.length) {
                $label.attr('for', newId);
            }
        });
        $nf.show();
    }



}