import $ from 'jquery';
import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ['permanent','allocations'];

    checkAll() {
        const checked = this.permanentTarget.checked;
        const checkboxes = $(this.allocationsTarget).find('[type="checkbox"]');
        checkboxes.each((index,el) => $(el).attr('checked', checked));
    }
}