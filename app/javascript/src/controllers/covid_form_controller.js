import {Controller} from "stimulus"

export default class extends Controller {
    static targets = ['status', 'positive', 'tested', 'secondVaccination'];

    connect() {
        this.statusTarget.addEventListener('change', this.handleStatusChange)
        this.handleStatusChange();
    }

    handleStatusChange = () => {
        const { value } = this.statusTarget;

        $(this.positiveTarget).toggle(value === 'recovered' || value === 'vaccinated')

        $(this.testedTarget).toggle(value === 'recovered');
        $(this.secondVaccinationTarget).toggle(value === 'vaccinated');
    }
}
