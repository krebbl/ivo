import $ from 'jquery';
import Pikaday from "pikaday";
import moment from 'moment';
import "moment/locale/de"
import "pikaday/css/pikaday.css"
import { Controller } from "stimulus"

export default class extends Controller {

    connect() {
        const $el = $(this.element);
        this.picker = new Pikaday({
            field: this.element,
            format: $el.attr('data-date-format') || "DD.MM.YYYY",
            i18n: moment.localeData("de")._config
        });

        const startDate = $el.attr('data-start-date');
        if (startDate) {
            this.picker.gotoDate(moment(startDate).toDate());
        }
    }

}