import $ from 'jquery';
import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ['text'];

    connect() {
        $(this.textTarget).dblclick((e) => {
            this.edit(e);
        });
    }

    edit(e) {
        e.preventDefault();
        const textBefore = this.textTarget.value;
        this.textTarget.readOnly = false;
        this.textTarget.focus();
        $(this.textTarget).blur(function (e) {
            if (textBefore != e.currentTarget.value) {
                e.currentTarget.form.submit();
            }
            e.currentTarget.readOnly = true;
        });
    }

}
