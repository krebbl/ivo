import 'sticky-table-headers'
import 'jquery-sticky-kit'
import { Controller } from "stimulus"

export default class extends Controller {

    connect() {
        $(this.element).click(() => {
            const ids = $('[name="command[ids][]"]:checked').map(function () {
                return this.value
            }).get().join(',');
            if (!ids) {
                alert("Nichts ausgewählt!");
            } else {
                this.openDialog(`/admin/dialogs/invoice?ids=${ids}`, 'Rechnung erstellen');
            }
        });

    }

    openDialog(url, title) {
        this.$modalDialog = $('#modalDialog');

        this.$modalDialog.find(".modal-title").html(title);
        this.$modalDialog.find(".modal-body").load(url, () => {
            this.$modalDialog.modal('show');
        });

    }

    closeDialog() {
        this.$modalDialog.modal('hide');
    }


}