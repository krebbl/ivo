import 'sticky-table-headers'
import 'jquery-sticky-kit'
import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ['header', 'footer', 'selectionInfo'];

    connect() {

        const header = this.headerTarget;
        setTimeout(() => {
            const offsetTop = $('#navSpacer').get(0).offsetHeight;
            $(this.headerTarget).stick_in_parent({offset_top: offsetTop});
            $(this.element).find('table').stickyTableHeaders({cacheHeaderHeight: true, fixedOffset: offsetTop + header.offsetHeight, zIndex: 999});
        },500);

        this.$groupForm = $(this.element).find('#groupForm');
        this.$allCheckbox = this.$groupForm.find('#command_all');
        this.$allCheckbox.change((e) => {
            this.toggleCheckboxes(e.currentTarget.checked)
        });
        this.$idCheckboxes = this.$groupForm.find('[name="command[ids][]"]');
        this.$idCheckboxes.change(() => {
             this.checkCheckboxes();
        });
        setTimeout(() => {
            this.updateSelectionInfo();
        }, 500);
    }

    toggleCheckboxes(checked) {
        this.$idCheckboxes.each(function(){
            this.checked = checked;
        });
        this.updateSelectionInfo();
    }

    checkCheckboxes() {
        const uncheckedBoxes = this.$idCheckboxes.not(':checked');
        this.$allCheckbox.get(0).checked = this.$idCheckboxes.length > 0 && uncheckedBoxes.length === 0;
        this.updateSelectionInfo();
    }

    updateSelectionInfo() {
        if(this.selectionInfoTarget) {
            const checkedBoxes = this.$idCheckboxes.filter(':checked');
            this.selectionInfoTarget.innerHTML = `${checkedBoxes.length} ausgewählt`;
        }
    }


}