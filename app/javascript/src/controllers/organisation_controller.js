import $ from 'jquery';
import { Controller } from "stimulus"

export default class extends Controller {
    static targets = ['info', 'select', 'createForm', 'createCheckbox'];

    connect() {
        this.loadInfo();
        this.toggleCreate();
    }

    loadInfo() {
        const type = $(this.element).attr('data-type');
        const id = this.selectTarget.value;
        if(id && !this.selectTarget.disabled) {
            $(this.infoTarget).load(`/api/v1/${type}/${id}.html`);
            $(this.infoTarget).show();
        } else {
            $(this.infoTarget).hide();
        }
    }

    toggleCreate() {
        const checkbox = this.createCheckboxTarget;
        $(this.createFormTarget).toggle(checkbox.checked);
        this.selectTarget.disabled = !!checkbox.checked;
        this.loadInfo();
    }
}