import 'sticky-table-headers'
import 'jquery-sticky-kit'
import { Controller } from "stimulus"

export default class extends Controller {

    connect() {
        this.$form = $("#groupForm");

        $(this.element).click((e) => {
            const ids = $('[name="command[ids][]"]:checked').map(function () {
                return this.value
            }).get().join(',');
            if (!ids) {
                alert("Nichts ausgewählt!");
            } else {
                this.triggerAction(ids, e);
            }
        });

    }

    triggerAction(ids, e) {
        this.$form.get(0).elements.command_name.value = $(e.currentTarget).attr("data-group-command");
        const confirmMessage = $(e.currentTarget).attr("data-command-confirm");
        if (confirmMessage) {
            const answer = confirm(confirmMessage);
            if (!answer) {
                return;
            }
        }
        this.$form.submit();
    }


}