document.addEventListener("DOMContentLoaded", function () {
    if (window.top !== window) {
        const pushSizeToTop = function () {
            window.top.postMessage(JSON.stringify({
                type: "resize",
                height: document.body.offsetHeight,
                width: document.body.offsetWidth
            }), "*")
        };
        var debounceTimeout;
        window.addEventListener("resize", function () {
            debounceTimeout && clearTimeout(debounceTimeout);
            debounceTimeout = setTimeout(function () {
                pushSizeToTop();
            }, 200);
        });
        pushSizeToTop();

        document.querySelectorAll("img").forEach(function (img) {
            img.addEventListener("load", pushSizeToTop);
        });
    }
});