import modernizr from 'modernizr';
import 'bootstrap'
import 'bootstrap/scss/bootstrap.scss'
import fontawesome from '@fortawesome/fontawesome'
import regular from '@fortawesome/fontawesome-free-regular'
import solid from '@fortawesome/fontawesome-free-solid'
import brands from '@fortawesome/fontawesome-free-brands'

window.$ = window.jQuery = jQuery;

fontawesome.library.add(regular);
fontawesome.library.add(solid);
fontawesome.library.add(brands);

import '../styles/application.scss'
import $ from 'jquery'

import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"

const application = Application.start();
const context = require.context("../src/controllers", true, /\.js$/);
application.load(definitionsFromContext(context));

function triggerReady() {
    $(document).trigger('bodyReady');
}

$(document).ready(triggerReady);
$(document).on('turbolinks:load', triggerReady);

function applyTooltips() {
    $('.tooltip').remove();
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
}

function applyCheckboxButtons() {
    $('[data-toggle="buttons"] input').each(function (index, el) {
        $(el.parentNode).toggleClass('active', el.checked)
    });
}

$(document).on('bodyReady', applyTooltips);
$(document).on('bodyReady', applyCheckboxButtons);

window.addEventListener("message", function (e) {
    try {
        var payload = JSON.parse(e.data);
        if (payload.type === "resize") {
            const iframe = document.querySelector('iframe[src="' + e.source.location.pathname + '"]')
            if (iframe) {
                iframe.setAttribute("height", payload.height + "px");
            }
        }
    } catch (e) {

    }
}, false);
