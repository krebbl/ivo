class SchoolClass < ApplicationRecord
  belongs_to :type, class_name: 'SchoolClassType', foreign_key: 'school_class_type_id'
  belongs_to :semester, class_name: 'SchoolSemester', foreign_key: 'start_semester'

  has_many :pupils, dependent: :nullify

  scope :with_type_and_semester, -> {includes(:type, :semester).joins(:semester, :type).order("abbreviation ASC", "start_date DESC")}
  scope :ordered, -> {includes(:type, :semester).joins(:semester, :type).order("abbreviation ASC", "start_date DESC")}
  default_scope -> {includes(:type, :semester).joins(:semester, :type)}
  delegate :name, to: :type
  delegate :abbreviation, to: :type

  def option_name
    if semester
      return "#{I18n.l semester.start_date, :format => "%B"} #{semester.short}"
    end

    "-"
  end

  def short_name
    if type
      return "#{abbreviation} #{semester.short}"
    end
    return "-"
  end

  def allocated_cws
    @allocated_cs ||= allocation.split(",").map{|a| a.strip.to_i}
  end

  def allocated?(cw)
    allocated_cws.include?(cw)
  end

  def allocation=(allocation)
    if allocation.kind_of? Array
      allocation = allocation.map {|v| v.length == 1 ? "0#{v}" : v}.join(",")
    end
    self[:allocation] = allocation
  end

  def self.current_cw
    cw = [Date.today.cweek.to_i, 1].max
    Time.now.wday == 0 ? cw + 1 : cw
  end

  def self.find_by_calendar_week(cw)
    cw = cw == 0 ? current_cw : cw
    cw = cw < 10 ? "0#{cw}" : cw

    where("POSITION(? in allocation) > 0", cw).or(includes(:type).where("school_class_types.permanent IS TRUE"))
  end

  def self.find_all_in_semester(semester)
      self.joins(:semester).where("start_date <= ?", semester.start_date)
          .where("(YEAR(?) - YEAR(school_semesters.start_date) + YEAR(?) - YEAR(school_semesters.stop_date)) <= duration", semester.start_date, semester.stop_date)
  end

end
