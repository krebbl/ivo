class Pupil < ApplicationRecord
  MEASLES_VACCINATION_OPTIONS = %w(vaccinated certificated not_possible none)

  include AddressAttributes

  strip_attributes only: [:firstname, :lastname]

  attr_accessor :school_class_type_id

  attr_accessor :create_school
  attr_accessor :create_company

  attr_accessor :new_school
  attr_accessor :new_company
  attr_accessor :school_semester_id

  has_one :address, as: :addressable
  has_one :deposit
  has_one :car, class_name: 'PupilCar'
  has_one :wifi, class_name: 'PupilWifi'

  has_many :parents, class_name: 'PupilParent'
  has_many :comments, class_name: 'PupilComment'

  belongs_to :company, optional: true
  belongs_to :school, optional: true
  belongs_to :school_class, optional: true
  belongs_to :county

  has_one :school_class_type, through: :school_class, :source => 'type'
  has_one :school_semester, through: :school_class, :source => 'semester'
  has_one :covid_info

  has_many :comments, class_name: 'PupilComment'
  has_many :bills, class_name: 'PupilBill'

  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :birthplace, presence: true
  validates :birthdate, presence: true
  validates :county_id, presence: true
  validates :nationality, presence: true
  validates :address, presence: true
  validates :arrival_day, presence: true

  validates_presence_of :measles_vaccination, on: :create
  validates_inclusion_of :measles_vaccination, in: Pupil::MEASLES_VACCINATION_OPTIONS + [nil]
  validates_associated :covid_info, on: :create

  validate :parents_under_age

  validates_associated :address
  validates_associated :parents

  validates_associated :new_company, if: -> {create_company == '1'}
  validates_associated :new_school, if: -> {create_school == '1'}

  accepts_nested_attributes_for :parents, :allow_destroy => true, reject_if: :all_blank
  accepts_nested_attributes_for :bills, :allow_destroy => true

  accepts_nested_attributes_for :car, :allow_destroy => true, reject_if: :all_blank
  accepts_nested_attributes_for :wifi, :allow_destroy => true, reject_if: :all_blank
  accepts_nested_attributes_for :deposit, :allow_destroy => true, reject_if: :all_blank
  accepts_nested_attributes_for :address, :allow_destroy => true
  accepts_nested_attributes_for :covid_info, :allow_destroy => true

  has_attached_file :photo, styles: {medium: "300x300>", thumb: "100x100>"}, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/

  scope :with_details, -> {includes(:school_class_type, :school_semester, :deposit, :car, :wifi)}

  before_save :save_new_organisations

  def school_class_type_id
    return @school_class_type_id if @school_class_type_id
    if school_class
      school_class.type.id
    end
  end

  def school_class_type_id=(id)
    @school_class_type_id = id
  end

  def male
    ret = super
    ret == nil ? false : ret
  end

  def fullname
    "#{firstname} #{lastname}"
  end

  def school_class_info
    school_class_type ? "#{school_class.short_name}" : ''
  end

  def deposit_info
    deposit ? "#{deposit.block} / #{deposit.nr}" : ''
  end

  def deposit_paid_in
    deposit && deposit.paid_in
  end

  def deposit_paid_out
    deposit && deposit.paid_out
  end

  def age
    birthdate ? ((Time.zone.now - birthdate.to_time) / 1.year.seconds).floor : 0
  end

  def minor?
    age < 18
  end

  def deposit_attributes=(attrs)
    assign_model_attributes(:deposit, attrs)
  end

  def wifi_attributes=(attrs)
    assign_model_attributes(:wifi, attrs)
  end

  def car_attributes=(attrs)
    assign_model_attributes(:car, attrs)
  end

  def covid_info_attributes=(attrs)
    assign_model_attributes(:covid_info, attrs)
  end

  def bills_attributes=(attrs)
    update_bills = []
    attrs.each do |index, b|
      if b['value'].present?
        bill = self.bills.find_or_create_by(cw: b['cw'])
        bill.assign_attributes(b)
        update_bills << bill
      else
        bill = self.bills.where(cw: b['cw']).first
        if bill
          bill.mark_for_destruction
          update_bills << bill
        end
      end
    end
    self.bills = update_bills
  end

  def parents_attributes=(parents_attrs)
    update_parents = []
    parents_attrs.each {|_, parent|
      unless parent['_destroy'] == '1'
        if parent['id'].present?
          p = self.parents.where(id: parent['id']).first
          if p
            p.assign_attributes(parent.except('id', '_destroy'))
            update_parents << p
          end
        else
          p = self.parents.build(parent.except('_destroy'))
          if p.attributes.any? do |_, value|
            value.present?
          end
            update_parents << p
          end
        end
      end
    }
    if minor? && update_parents.empty?
      update_parents << self.parents.first if self.parents.any?
    end
    self.parents = update_parents
  end

  def new_school=(attr)
    @new_school = School.new(attr)
  end

  def new_school
    @new_school || School.new
  end

  def new_company=(attr)
    @new_company = Company.new(attr)
  end

  def new_company
    @new_company || Company.new
  end

  def public_id
    Base64.encode64("#{firstname}|#{lastname}|#{passwd}").gsub("\n", "!")
  end

  def school_semester_id=(id)
    @school_semester_id = id
    unless id&.blank?
      semester = SchoolSemester.find(id)
      if semester && school_class_type_id
        self.school_class = SchoolClass.find_or_create_by(start_semester: semester.id, school_class_type_id: school_class_type_id)
      end
    end
  end

  def school_semester_id
    return @school_semester_id if @school_semester_id
    if school_class&.start_semester
      school_class.start_semester.id
    end
  end

  def self.find_by_public_id(public_id)
    decoded = Base64.decode64(public_id.gsub('!', '\n'))
    splitted = decoded.split('|')

    self.where("lower(firstname) = ? AND lower(lastname) = ? AND passwd = ?", splitted[0].downcase, splitted[1].downcase, splitted[2].upcase).first
  end

  private

  def assign_model_attributes(relation, attrs)
    is_blank = attrs.all? {|_, value| value.blank?}
    model = self.send(relation)
    if is_blank
      model.destroy if model
    elsif model
      model.assign_attributes(attrs)
    else
      self.send("build_#{relation}", attrs)
    end
  end

  def save_new_organisations
    @new_company.save && self.company = @new_company if create_company == '1'
    @new_school.save && self.school = @new_school if create_school == '1'
    @new_school_class.save if @new_school_class
  end

  def parents_under_age
    errors.add(:parents, :blank) if parents.select {|p| p.valid?}.empty? && age < 18
  end

end
