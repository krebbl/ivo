module AddressAttributes
  def address_attributes= address_hash
    current_address = self.address || self.build_address
    current_address.addressable = self
    current_address.assign_attributes(address_hash)
    self.address = current_address
  end
end