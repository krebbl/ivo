class SchoolSemester < ApplicationRecord
  validates_presence_of :start_date, :stop_date

  def short
    start_date.strftime('%y')
  end

  def name
    sem = start_date.year < stop_date.year ? 'Winters.' : 'Sommers.'

    "#{sem} #{start_date.year}"
  end

  def self.latest
    return self.where("start_date <= ?", Date.today).order(stop_date: :DESC).first
  end
end
