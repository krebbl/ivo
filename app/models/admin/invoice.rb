class Admin::Invoice
  include ActiveModel::Model
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  attr_accessor :nr, :from, :to, :address_id, :pupil_ids
end