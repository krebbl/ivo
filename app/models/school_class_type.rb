class SchoolClassType < ApplicationRecord
  has_many :school_classes, dependent: :destroy

  validates_uniqueness_of :abbreviation
end
