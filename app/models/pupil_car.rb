class PupilCar < ApplicationRecord

  def short_title
    "#{name} / #{color} - #{sign}"
  end

end
