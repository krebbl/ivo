class School < Organisation
  has_many :pupils, dependent: :nullify
  has_many :comments, as: :commentable
end
