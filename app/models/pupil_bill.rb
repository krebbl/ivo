class PupilBill < ApplicationRecord
  belongs_to :pupil

  def year
    if cw
      cw.split("-")[0].to_i
    end
  end

  def cweek
    if cw
      cw.split("-")[1].to_i
    end
  end
end
