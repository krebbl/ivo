class PupilParent < ApplicationRecord
  has_one :address, as: :addressable

  accepts_nested_attributes_for :address, :allow_destroy => true

  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :phone_nr, presence: true, if: :new_record?
  # validates_associated :address, if:

  default_scope {order(updated_at: :ASC)}

  def address_attributes=(attrs)
    if attrs['_destroy'] == "1"
      self.address.destroy if self.address
      self.address = nil
    else
      adress = self.address || self.build_address
      adress.assign_attributes(attrs.except('_destroy'))
      adress.addressable = self
    end
  end

end
