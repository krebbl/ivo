class Organisation < ApplicationRecord
  include AddressAttributes

  self.abstract_class = true

  has_one :address, as: :addressable, required: true

  accepts_nested_attributes_for :address

  scope :with_address, -> {includes(:address)}

  validates :name, presence: true
  validates_associated :address

  def option_name
    ret = name

    if address
      ret += " (#{address.city})"
    end

    ret
  end
end