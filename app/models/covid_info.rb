class CovidInfo < ApplicationRecord
  extend Enumerize

  belongs_to :pupil

  enumerize :status, in: [:unknown, :recovered, :vaccinated, :nothing], default: :unknown

  validates_presence_of :tested_on, if: -> { status.recovered? }
  validates_presence_of :first_vaccination_on, if: -> { status.vaccinated? }
end
