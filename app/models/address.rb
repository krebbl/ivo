class Address < ApplicationRecord
  belongs_to :addressable, polymorphic: true

  validates :city, presence: true
  validates :zipcode, presence: true, numericality: {only_integer: true}
  validates :street, presence: true

  def _destroy
    self.persisted? || [:city, :zip_code, :street, :home_nr, :mobile_nr, :fax_nr].any? {|key| self[key].present?} ? '0' : '1'
  end
end
