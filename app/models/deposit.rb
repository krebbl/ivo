class Deposit < ApplicationRecord
  validates :block, presence: true
  validates :nr, presence: true
  validates :value, presence: true

  validates :paid_in, presence: true
end
