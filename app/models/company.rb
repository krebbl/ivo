class Company < Organisation
  has_many :pupils, dependent: :nullify
  has_many :comments, as: :commentable
end
