class User < ApplicationRecord
  has_secure_password
  belongs_to :group

  validates_uniqueness_of :username

  def authenticate(password)
    if Digest::SHA1.hexdigest("9e4b891b2e6f602d643cbc43b4bc16c00ccfa369" + password) == self.password_digest
      # self.password = password
      # self.password_confirmation =
      return true
    end
    super(password)
  end

  def is_admin?
    group_id == 1
  end

end
