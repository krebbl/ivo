module PupilFilter
  def self.filter(params, base = Pupil)
    pupils = base
    if params[:semester].is_a? SchoolSemester
      active_school_classes = params[:semester] ? SchoolClass.find_all_in_semester(params[:semester]) : []
      pupils = pupils.where(school_class: active_school_classes.map {|sc| sc.id}).or(pupils.where(permanent: true))
    end

    unless params[:cw].blank?
      cw_school_classes = SchoolClass.find_by_calendar_week(params[:cw].to_i)
      pupils = pupils.where(school_class: cw_school_classes.map {|sc| sc.id})
      pupils = pupils.or(pupils.where(permanent: true)).or(pupils.where(school_class_id: nil))
    end

    unless params[:search].blank?
      ids = []
      search = {search: "%#{params[:search].downcase}%"}
      cars = PupilCar.where("LOWER(name) LIKE :search OR LOWER(sign) LIKE :search OR LOWER(color) LIKE :search", search)
      ids += cars.map {|car| car.pupil_id} if cars.any?
      deposits = Deposit.where("LOWER(block) LIKE :search", search)
      ids += deposits.map {|deposit| deposit.pupil_id} if deposits.any?

      pupils = pupils.where("LOWER(firstname) LIKE :search OR LOWER(lastname) LIKE :search", search).or(pupils.where(id: ids))
    end

    case params[:status]
    when :checked_in.to_s
      pupils = pupils.where("checked_out IS NULL")
    when :checked_out.to_s
      pupils = pupils.where("checked_out IS NOT NULL")
    else
      # do nothing
    end

    case params[:gender]
    when :male.to_s
      pupils = pupils.where("male = 1")
    when :female.to_s
      pupils = pupils.where("male = 0")
    else
      # do nothing
    end


    case params[:deposit]
    when :not_paid_in.to_s
      pupils = pupils.joins("LEFT OUTER JOIN deposits ON deposits.pupil_id = pupils.id ").where("deposits.paid_in IS NULL")
    when :not_paid_out.to_s
      pupils = pupils.joins("LEFT OUTER JOIN deposits ON deposits.pupil_id = pupils.id ").where("deposits.paid_in IS NOT NULL").where("deposits.paid_out IS NULL")
    when :paid_out.to_s
      pupils = pupils.joins("LEFT OUTER JOIN deposits ON deposits.pupil_id = pupils.id ").where("deposits.paid_in IS NOT NULL").where("deposits.paid_out IS NOT NULL")
    else
      # do nothing
    end

    unless params[:company].blank?
      pupils = pupils.where(company: params[:company])
    end

    unless params[:county].blank?
      pupils = pupils.where(county: params[:county])
    end

    unless params[:school_class_type].blank?
      school_classes = SchoolClass.where(school_class_type_id: params[:school_class_type]).select(:id)
      pupils = pupils.where(school_class: school_classes.map(&:id))
    end

    pupils
  end
end
