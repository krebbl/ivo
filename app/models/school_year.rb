class SchoolYear
  include ActiveModel::Validations
  include ActiveModel::Conversion
  include ActiveModel::AttributeAssignment
  extend ActiveModel::Naming

  attr_accessor :summer_semester
  attr_accessor :winter_semester

  attr_accessor :school_class_types

  validate :validate_semester

  def initialize(start_date = Date.new)
    @start_date = start_date
  end

  def summer_semester=(hash)
    semester = summer_semester
    semester.assign_attributes(hash)
  end

  def summer_semester
    @summer_semester ||= SchoolSemester.new({
                                                start_date: @start_date + 7.months + 1.day,
                                                stop_date: @start_date + 12.months
                                            })
  end

  def winter_semester=(hash)
    semester = winter_semester
    semester.assign_attributes(hash)
  end

  def winter_semester
    @winter_semester ||= SchoolSemester.new({
                                                start_date: @start_date,
                                                stop_date: @start_date + 7.months
                                            })
  end

  def school_class_types
    @school_class_types ||= []
  end

  def school_class_types=(array)
    @school_class_types = array
  end

  def validate_semester
    errors.add(:summer_semester, :invalid, message: "is invalid") unless @summer_semester.valid?
    errors.add(:winter_semester, :invalid, message: "is invalid") unless @winter_semester.valid?
    if @summer_semester.valid? && @winter_semester.valid?
      errors.add(:winter_semester, :invalid, message: 'muss vor Sommersemester enden') if @winter_semester.stop_date > @summer_semester.start_date
    end
  end

  def save
    if valid?
      @winter_semester.save
      @summer_semester.save

      @school_class_types.each do |id|
        school_class_type = SchoolClassType.find(id)
        if school_class_type
          SchoolClass.create({
                                 type: school_class_type,
                                 start_semester: @winter_semester
                             })
        end
      end

      true
    else
      false
    end
  end

end
