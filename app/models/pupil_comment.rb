class PupilComment < ApplicationRecord
  belongs_to :user
  belongs_to :pupil, counter_cache: :comments_count

  validates_presence_of :text
end
