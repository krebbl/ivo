module ApplicationHelper
  def info_icon(object = true, attributes = {}, &block)
    content = capture(&block)
    css_class = ['info-icon']
    css_class << 'disabled' unless object

    attributes = {class: css_class.join(' ')}.merge(attributes)

    title = object.try(:short_title) || attributes[:title]
    if title && object
      attributes[:data] ||= {}
      attributes[:data][:toggle] = 'tooltip'
      attributes[:title] = title
    end

    content_tag(:span, attributes) do
      content
    end
  end

  def class_name(hash = {})
    ret = []
    hash.each do |key, value|
      if !!value
        ret << key.to_s
      end
    end
    ret.join(' ')
  end

  def pupil_download_path(pupil, inline = false)
    path = registration_path(pupil.public_id) + ".pdf"
    path << "?inline" if inline
    path
  end

  def current_cw
    SchoolClass.current_cw
  end

  def calendar_weeks
    (0..51).to_a.map do |cw|
      ret = (current_cw + cw - 12) % 52
      ret > 0 ? ret : 52
    end
  end
end
