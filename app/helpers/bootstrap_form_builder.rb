class BootstrapFormBuilder < ActionView::Helpers::FormBuilder
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::RenderingHelper

  include ActionView::Context
  delegate :content_tag, :concat, to: :@template

  def form_group(*args, &block)
    field = args[0]
    title = args[1]
    options = args[2] if args.length > 2
    options = {col: 4}.merge(options || {})

    content = @template.capture(&block) if block_given?
    label_style = case title
                    when ''
                      'invisible d-none-xs'
                    when false
                      'd-none'
                    else
                      ''
                  end
    content_tag(:div, class: "form-group col-md-#{options[:col]} col-sm", data: options[:data]) do
      concat label(field, title, class: "small #{label_style}")
      concat content
      concat content_tag(:div, object.errors[field].first, class: 'invalid-feedback') if object.errors && object.errors[field].any?
    end
  end

  def address_fields(field, object, &block)
    fields_for field, object || self.object.send(field), &block
  end

  def date_field(field, options = {})
    val = object.try(field)
    data = {controller: 'date'}.merge(options[:data] || {})
    start_date = options.delete(:start_date)
    if start_date && start_date.method(:rfc2822)
      data["start-date"] = start_date.rfc2822.to_s unless val
    end
    opt = {autocomplete: 'off', placeholder: 'TT.MM.JJJJ', value: val ? I18n.localize(val) : ''}.merge(options)
    opt[:data] = data
    text_field(field, opt)
  end

  def text_field(field, options = {})
    opt = {
        class: 'form-control form-control-sm'
    }.merge(options)

    super field, opt
  end

  def password_field(field, options = {})
    opt = {
        class: 'form-control form-control-sm'
    }.merge(options)

    super field, opt
  end

  def text_area(field, options = {})
    opt = {
        class: 'form-control form-control-sm'
    }.merge(options)

    super field, opt
  end

  def select(method, choices = nil, options = {}, html_options = {}, &block)
    opt = {
        class: 'form-control form-control-sm'
    }.merge(html_options)

    super method, choices, options, opt, &block
  end

end
