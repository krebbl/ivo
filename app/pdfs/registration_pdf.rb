require 'prawn'

class RegistrationPdf < Prawn::Document

  def initialize(pupil)
    super(top_margin: cm(2.0), left_margin: cm(2.0))

    @pupil = pupil
    @cursor_x = 0

    header
    personal
    parents
    apprenticeship
    dorm

    move_down 12

    text "Die/Der Unterzeichner bestätigen die Richtigkeit der o.g. persönlichen Angaben und erkennen die aktuell gültige Internatsvereinbarung und Hausordnung in vollem Umfang an. Sollten Punkte der Internatsvereinbarung und/oder der Hausordnung unwirksam werden, berührt dies die Wirksamkeit der übrigen Punkte nicht.", size: 9

    move_down 12

    text "Die Mitteilungspflicht bei Änderungen der o.g. persönlichen Daten obliegt den Unterzeichnern.", size: 9

    move_down 5

    signatures

  end

  def header
    text "Drachenberg 4b, 98617 Meiningen", size: 7, style: :bold
    move_down 5

    text "Anmeldung für das Internat der Meininger Berufsschulen", size: 14, style: :bold

    move_down 4

    if @pupil
      text "Bitte 2 (zwei!) Passbilder beifügen bzw. nachreichen!", size: 10
    else
      text "Bitte in Blockschrift ausfüllen und 2 (zwei!) Passbilder beifügen bzw. nachreichen!", size: 10
    end

    move_down 4
  end


  def personal
    legend "Angaben zur Person und Adresse"
    personal_name
    personal_birth
    address(@pupil.address)
    personal_county
  end

  def parents
    parents = get_parents
    if parents.count > 0
      legend "Angaben zu den Gesetzlichen Vertretern"
      parents.each do |p|
        parent(p)
      end
    end
  end

  def apprenticeship
    legend "Angaben zur Ausbildung"
    simple_field cm(9.2),"Schule", @pupil.try(:school).try(:name)
    simple_field cm(8.2),"Ausbildungsrichtung (Abkürzung)", @pupil.try(:school_class_info)

    new_row
    simple_field cm(17.6), "Firma", @pupil.try(:company).try(:name)
    new_row
    address @pupil.try(:company).try(:address)
  end

  def dorm
    legend "Angaben zur Internatsnutzung"
    simple_field cm(3), "Belegungstyp", @pupil.permanent ? 'Dauerbelgegung' : 'Turnusbelegung'
    date_field(@pupil.arrival_day, "Anreise am")
    simple_field cm(11.2), "Angaben zum Masernschutzgesetz", I18n.t("activerecord.attributes.pupil.measles_vaccination_#{@pupil.measles_vaccination}"), 9
    new_row
  end

  def signatures
    move_down 8
    simple_field(cm(6), "Ort")
    simple_field(cm(4), "Datum")
    new_row

    move_down 2

    signature_field(cm(6), "Unterschrift des Auszubildenden")

    if @pupil.age < 18
      parents = get_parents
      parents.each_with_index do |p, index|
        signature_field(cm(5), "Unterschrift des Gesetzlichen Vertreters #{index + 1}")
      end
    end
  end

  def address(address, include_phone_nr = true)
    if address
      simple_field(cm(13.4), 'Straße, PLZ, Ort', "#{address.street}, #{address.zipcode} #{address.city}")
      if include_phone_nr
        simple_field(cm(4), 'Telefonnummer', "#{address.home_nr}")
      end
    else
      simple_field(cm(13.4), 'Straße, PLZ, Ort', "")
    end
    new_row
  end

  def parent(parent)
    if parent.address
      simple_field(cm(4), 'Name', parent.lastname)
      simple_field(cm(4), 'Vorname', parent.firstname)
      simple_field(cm(5), 'Telefonnummer', parent.phone_nr)
      new_row
      address(parent.address, !parent.address.home_nr.blank? || !parent.address.mobile_nr.blank? || !parent.address.fax_nr.blank?)
    else
      simple_field(cm(4), 'Name', parent.lastname)
      simple_field(cm(4), 'Vorname', parent.firstname)
      simple_field(cm(5), 'Telefonnummer', parent.phone_nr)
      simple_field(cm(3.2), 'Adresse', "siehe oben")
      new_row
    end
  end

  def personal_name
    simple_field(cm(8.2), 'Name', @pupil.lastname)
    simple_field(cm(9.2), 'Vorname', @pupil.firstname)
    new_row
  end

  def personal_birth
    date_field(@pupil.birthdate, 'Geburtsdatum')

    simple_field(cm(6), 'Geburtsort', @pupil.birthplace)
    simple_field(cm(4), 'Nationalität', I18n.t("nationality.#{@pupil.nationality}"))
    simple_field(cm(4), 'Geschlecht', @pupil.male ? 'männlich' : 'weiblich')

    new_row
  end

  def personal_county

    simple_field(cm(9.2), 'Landkreis', @pupil.county.name)
    simple_field(cm(8.2), 'Bundesland', @pupil.county.state.name)
    new_row
  end

  def small_heading(text)
    text text, size: 8, style: :bold
  end

  def simple_field(width, label, value = '', font_size = 10)
    field(width, label, value, :left, font_size)

    move_to_next_field(width)
  end

  def legend(label)
    @legend_number ||= 1
    move_down 0

    self.line_width = 1.5
    bounding_box([1, cursor - 8], :width => 15) do
      stroke_rectangle [0,0], 15, 15
      move_down 4
      text(@legend_number.to_s, style: :bold, size: 10, align: :center)
    end
    draw_text(label + ":", at: [22, cursor + 5], style: :bold, size: 10)


    @legend_number += 1
  end

  def field(width, label, value, align = :left, font_size = 10)
    cursor_before = cursor

    bounding_box([@cursor_x + 3.5, cursor - 5], :width => width - 6) do
      text(label.to_s, align: align, size: 6)
      move_down 3 + (10 - font_size)
      if value.present?
        text(value.to_s, align: align, size: font_size)
      else
        move_down 11.5
      end
    end

    stroke do
      stroke_color '000000'
      line_width 0.5
      # move_down 2
      horizontal_line(@cursor_x + 0.5, @cursor_x + width + 0.5)
      move_down -27

      vertical_line(cursor - 5.5, cursor - 27.2, at: @cursor_x + 0.5)
      vertical_line(cursor - 5.5, cursor - 27.2, at: @cursor_x + width + 0.5)
    end

    move_cursor_to cursor_before
  end

  def signature_field(width, label)
    cursor_before = cursor
    move_down 30
    stroke do
      stroke_color '000000'
      line_width 0.5
      horizontal_line(@cursor_x, @cursor_x + width)
    end
    bounding_box([@cursor_x, cursor], :width => width) do
      move_down 2
      text(label.to_s, align: :center, size: 7)
    end
    move_cursor_to(cursor_before)
    move_to_next_field(width)
  end

  def date_field(date, label)
    number_field({
        Tag: date.day,
        Monat: date.month,
        Jahr: date.year
                 }, label, cm(1.0))

  end

  def move_to_next_field(width = 0)
    @cursor_x += width + cm(0.2)
  end

  def number_field(number_hash, label, num_width)
    cursor_before = cursor

    total_width = number_hash.values.count * num_width

    bounding_box([@cursor_x + 3, cursor + 2.5], :width => total_width - 6) do
      text(label.to_s, align: :center, size: 6)
    end

    move_cursor_to cursor_before

    number_hash.each do |key, value|
      field(num_width, key, value, :center)
      @cursor_x += num_width
    end

    move_to_next_field
  end

  def new_row
    @cursor_x = 0
    move_down(33)
  end

  def cm(cm)
    cm * 0.393701 * 72
  end

  def get_parents
    parents = @pupil.parents
    if parents.length > 0
      parents = parents[0..0] if @pupil.age > 17
    end
    parents
  end


end
