require 'write_xlsx'

class XLS

  def initialize (sheet_name = 'Sheet1')
    @io = StringIO.new
    @workbook = WriteXLSX.new(@io)
    @worksheet = @workbook.add_worksheet(sheet_name)
  end

  def render
    @workbook.close
    @io.string
  end

  protected

  def format_cell(row, column, opt)
    @worksheet.row(row).set_format(column, Spreadsheet::Format.new(opt))
  end

  def insert(row, column, text, style = nil)
    format = @workbook.add_format
    format.set_font 'Calibri'
    format.set_size 11
    if style
      style.each do |key, value|
        format.send("set_#{key}", value)
      end
    end
    @worksheet.write(row, column, text, format)
  end

end