require 'write_xlsx'

class PupilListXLS < XLS

  def initialize(ids)
    super('Schülerübersicht')
    @ids = ids || []

    @cols = {
        room: -> do
          self.room
        end,
        school_class: -> {self.school_class_info},
        subclass: -> { self.subclass },
        lastname: -> {self.lastname},
        firstname: -> {self.firstname},
        rent_on_account: -> {self.rent_on_account ? "X" : ''},
        permanent: -> {self.permanent ? 'X' : ''},
        male: -> {self.male ? 'M' : 'W'},
        birthdate: -> {self.birthdate},
        is_adult: -> {self.age > 17 ? 'JA' : 'NEIN'},
        min_to_arrive: -> {self.min_to_arrive},
        min_to_depart: -> {self.min_to_depart},
        address_zipcode: -> {self.address ? self.address.zipcode : '-'},
        address_city: -> {self.address ? self.address.city : '-'},
        school: -> {self.school ? self.school.name : '-'},
        company: -> {self.company ? self.company.name : '-'},
        deposit_block: -> {self.deposit ? self.deposit.block : ''},
        deposit_nr: -> {self.deposit ? self.deposit.nr: ''},
        deposit_paid_in: -> {self.deposit ? self.deposit.paid_in: ''},
        deposit_paid_out: -> {self.deposit ? self.deposit.paid_out : ''},
        car_sign: -> {self.car ? self.car.sign : ''}
    }

  end

  def render
    # @worksheet.set_column(0, 0, 20)
    pupils = Pupil.where({id: @ids}).joins(:school_class).includes(:deposit, :company, :school, :address, :bills)

    @cols.keys.each_with_index do |key, col|
      splitted = key.to_s.split("_")
      first_key = splitted.shift
      scope = 'pupil'
      k = key
      if first_key.in?(%w(address deposit))
        scope = first_key
        k = splitted.join("_")
      end
      insert(0, col, I18n.translate("activerecord.attributes.#{scope}.#{k}"), {bold: true})
    end

    start_date = DateTime.now - 51.weeks
    current_time = start_date
    (0..51).each do |i|
      insert(0, @cols.length + i, "M #{current_time.strftime('%W')}.KW #{current_time.strftime('%Y')}", {bold: true})
      current_time += 1.week
    end

    pupils.each_with_index do |pupil, index|
      @cols.keys.each_with_index do |key, col|
        insert(index + 1, col, pupil.instance_exec(&@cols[key]), {})
      end
      current_time = start_date
      bills = pupil.bills.order(cw: :desc).limit(52).to_a
      (0..51).each do |i|
        cw = current_time.strftime("%W")
        cw = "0#{cw}" if cw.length == 1
        bill = bills.detect {|b| b.cw == "#{current_time.strftime("%Y")}-#{cw}"}
        if bill
          insert(index + 1, @cols.length + i, bill.value, {bold: false})
        end
        current_time = current_time + 1.week
      end
    end


    @workbook.close
    @io.string
  end

  private

end
