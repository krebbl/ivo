require 'write_xlsx'

class InvoiceXLS < XLS
  include ActionView::Helpers::NumberHelper

  def initialize(pupils, address, from, to, nr = '')
    super()
    @nr = nr
    @address = address
    @from = from
    @to = to
    @pupils = pupils || []
  end

  def render
    @worksheet.margin_right = 0.59
    @worksheet.set_column(0, 0, 20)

    insert(0, 0, "Internat der Meininger Berufsschulen")
    insert(1, 0, "Am Drachenberg 4b, 98617 Meiningen")


    if @address
      contact = @address.contact
      @current_row = 4
      if !contact || contact.blank?
        contact = [
            @address.addressable.name,
            @address.street,
            @address.zipcode + " " + @address.city
        ].join("\n")
      end

      lines = contact.split("\n")
      lines.each do |line|
        insert @current_row, 0, line
        @current_row += 1
      end

    end


    insert(9, 5, "Meiningen, #{Time.now.strftime('%d.%m.%Y')}", {horizontal_align: :right})

    insert(11, 0, 'Rechnung', {bold: 1})
    insert(11, 1, @nr, {horizontal_align: :right, bold: 1})
    insert(11, 2, "- #{Time.now.year}", {horizontal_align: :left, bold: 1})

    insert(13, 0, 'Internatsgebühren Ihrer Auszubildenden')
    insert(15, 0, 'Wir berechnen Ihnen für die Unterbringung Ihrer Auszubildenden im Internat wie folgt: ')

    @current_row = 17

    invoice_cols = {}
    @pupils.each_with_index do |pupil, index|
      bills = pupil.bills.where("cw >= ? AND cw <= ?", to_cw(@from), to_cw(@to))
      invoice_cols['names'] ||= []
      invoice_cols['names'] << "#{pupil.lastname}, #{pupil.firstname}"
      iterate_from_to(@from, @to) do |current|
        cw = to_cw(current)
        bill = bills.detect {|b| b.cw == to_cw(current)}
        value = bill ? (bill.value || "0").to_f : 0
        invoice_cols[cw] ||= []
        invoice_cols[cw] << value
      end
    end

    start_row = @current_row + 1
    invoice_cols.keys.each do |key|
      if key != 'names'
        sum = invoice_cols[key].reduce(:+)
        if sum == 0
          invoice_cols.delete(key)
        end
      end
    end
    number_style = {num_format: '#,##0.00_-[$€]', border: 1}
    calendar_cols = invoice_cols.keys.length - 1
    invoice_cols.keys.each_with_index do |key, index|
      if key == 'names'
        invoice_cols[key].each_with_index do |name, row_index|
          insert(@current_row + row_index + 1, 0, name, {border: 1})
        end
      else
        s = key.split("-")
        insert(@current_row, index, "#{s[1]}. KW #{s[0][2..3]}", {border: 1, horizontal_align: :right})
        invoice_cols[key].each_with_index do |value, row_index|
          insert(@current_row + row_index + 1, index, value, number_style)
        end
      end
    end
    end_row = start_row + invoice_cols['names'].length

    @current_row += invoice_cols['names'].length + 2

    insert(@current_row, 0, 'Gesamtbetrag:', {bold: 1})
    insert(@current_row, 1, "=SUM(B#{start_row + 1}:#{(calendar_cols + 65).chr(Encoding::UTF_8)}#{end_row + 1})", {bold: 1, num_format: '#,##0.00_-[$€]'})

    @current_row += 2

    insert(@current_row, 0, 'Zahlbar: sofort', {bold: 1})

    @current_row += 2

    insert(@current_row, 0, 'Bitte überweisen Sie den o. g. Betrag auf folgendes Konto:')

    bank_data = {
        'Bankinstitut' => 'Rhön-Rennsteig-Sparkasse Meiningen',
        'IBAN' => 'DE12840500001305004635',
        'BIC' => 'HELADEF1RRS',
        'Zahlungsgrund' => '24001 11010',
        'Kontoinhaber' => 'Landratsamt Schmalkalden Meiningen'
    }

    @current_row += 2

    bank_data.each do |key, value|
      insert(@current_row, 0, key)
      insert(@current_row, 1, value)
      @current_row += 1
    end

    @current_row += 4
    insert(@current_row, 0, "A. Krejpowicz", {bold: 1, horizontal_align: :center})

    @current_row += 1
    insert(@current_row, 0, "Internatsleiter", {bold: 1, horizontal_align: :center, size: 8})

    @workbook.close
    return @io.string
  end

  private

  def iterate_from_to(from, to)
    f = from
    while f <= to
      yield f
      f = f + 1.week
    end
  end

  def to_cw(time)
    cw = time.to_date.cweek
    cw = cw < 10 ? "0#{cw}" : cw
    "#{time.strftime('%Y')}-#{cw}"
  end

end